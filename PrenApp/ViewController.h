//
//  ViewController.h
//  PrenApp
//
//  Created by HSLU on 13/11/14.
//  Copyright (c) 2014 Hannes Mathis HSLU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AVFoundation/AVFoundation.h"

@interface ViewController : UIViewController <NSStreamDelegate, UIPickerViewDataSource, UIPickerViewDelegate> {

    NSInputStream	*inputStream;
    NSOutputStream	*outputStream;
    NSTimer *stopWatchTimer;
    NSDate * startDate;
    AVAudioPlayer *audioPlayer;
}

@property (weak, nonatomic) IBOutlet UISegmentedControl *ballAmount;

@property (weak, nonatomic) IBOutlet UITextField *textFieldIP;
@property (weak, nonatomic) IBOutlet UILabel *elapsedTime;
@property (weak, nonatomic) IBOutlet UIButton *connectButton;
@property (weak, nonatomic) IBOutlet UISwitch *sliderToggleSwitch;
@property (weak, nonatomic) IBOutlet UIButton *buttonWettbewerb;
@property (weak, nonatomic) IBOutlet UIButton *buttonEinzeltest;
@property (weak, nonatomic) IBOutlet UIButton *buttonVorbereiten;
@property (weak, nonatomic) IBOutlet UIButton *buttonLaden;
@property (weak, nonatomic) IBOutlet UIButton *buttonKlappeAuf;
@property (weak, nonatomic) IBOutlet UIButton *buttonKlappeZu;


@property (weak, nonatomic) IBOutlet UISlider *Slider;
@property (weak, nonatomic) IBOutlet UILabel *sliderLabel;

@property (weak, nonatomic) IBOutlet UISlider *SliderAngle;
@property (weak, nonatomic) IBOutlet UILabel *SliderAngleLabel;


@property (strong, nonatomic)          NSArray *dataSourceArray;

@property (weak, nonatomic) IBOutlet UILabel *checkBoxConnect;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinningConnect;

@property (weak, nonatomic) IBOutlet UIPickerView *pickerViewAction;
@property (weak, nonatomic) IBOutlet UITextView *stateTextView;

@property (nonatomic) BOOL error;
@property (nonatomic) BOOL connecting;
@property (nonatomic) BOOL gestartet;

- (IBAction)sliderValueChanged:(id)sender;
- (IBAction)sliderValueAngleChanged:(id)sender;
- (IBAction)connectButtonClicked:(UIButton *)sender;
- (IBAction)buttonWettbewerbClicked:(id)sender;
- (IBAction)buttonVorbereitenClicked:(id)sender;
- (IBAction)buttonLadenClicked:(id)sender;
- (IBAction)buttonKlappeAufClicked:(id)sender;
- (IBAction)buttonKlappeZuClicked:(id)sender;
- (IBAction)playSound;

@property (nonatomic, retain) NSInputStream *inputStream;
@property (nonatomic, retain) NSOutputStream *outputStream;
@property (nonatomic, retain) NSString *actionString;


- (void) sendString:(NSString*)text;
- (void) initNetworkCommunication;
- (void)startTimer;
- (void)stopTimer;
- (void)connected;
- (void)disconnect;

@end

