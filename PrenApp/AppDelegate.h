//
//  AppDelegate.h
//  PrenApp
//
//  Created by HSLU on 13/11/14.
//  Copyright (c) 2014 Hannes Mathis HSLU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

