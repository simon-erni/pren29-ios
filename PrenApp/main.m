//
//  main.m
//  PrenApp
//
//  Created by HSLU on 13/11/14.
//  Copyright (c) 2014 Hannes Mathis HSLU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
