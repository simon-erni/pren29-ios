//
//  ViewController.m
//  PrenApp
//
//  Created by HSLU on 13/11/14.
//  Copyright (c) 2014 Hannes Mathis HSLU. All rights reserved.
//

#import "ViewController.h"


@implementation ViewController

@synthesize inputStream, outputStream;


- (void)viewDidLoad {
    [super viewDidLoad];
    self.error = YES;
    
    
    
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/tada.mp3", [[NSBundle mainBundle]resourcePath]]];
    
    NSError *error;
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    audioPlayer.numberOfLoops = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    if ([_textFieldIP isFirstResponder] && [touch view] != _textFieldIP) {
        [_textFieldIP resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
}

- (IBAction)connectButtonClicked:(UIButton *)sender{
    if (!self.connecting)
    {
        self.checkBoxConnect.hidden = YES;
        self.connecting = YES;
        [self.connectButton setTitle:@"Stop" forState:UIControlStateNormal];
        [self.spinningConnect startAnimating];
        [self initNetworkCommunication];
    }
    else
    {
        [self.connectButton setTitle:@"Verbinden" forState:UIControlStateNormal];
        [self disconnect];
    }
}

- (void) initNetworkCommunication{
    
    NSString *host = self.textFieldIP.text;
    
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)host, 1337, &readStream, &writeStream);
    
    CFReadStreamSetProperty(readStream, kCFStreamPropertyShouldCloseNativeSocket, kCFBooleanTrue);
    CFWriteStreamSetProperty(writeStream, kCFStreamPropertyShouldCloseNativeSocket, kCFBooleanTrue);
    
    inputStream = (__bridge NSInputStream *)readStream;
    outputStream = (__bridge NSOutputStream *)writeStream;
    
    
    [self open];
    
    return;
}

- (void)open {
    [inputStream setDelegate:self];
    [outputStream setDelegate:self];
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop]
                           forMode:NSDefaultRunLoopMode];
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop]
                            forMode:NSDefaultRunLoopMode];
    [inputStream open];
    [outputStream open];
}

- (void)close {
    [inputStream close];
    [outputStream close];
    [inputStream removeFromRunLoop:[NSRunLoop currentRunLoop]
                           forMode:NSDefaultRunLoopMode];
    [outputStream removeFromRunLoop:[NSRunLoop currentRunLoop]
                            forMode:NSDefaultRunLoopMode]; [inputStream setDelegate:nil]; [outputStream setDelegate:nil];
    inputStream = nil;
    outputStream = nil;
}

- (void) sendString:(NSString*)text{
    
    [self stopBlinkingLabel:self.elapsedTime];
    NSString *sendString = [NSString stringWithFormat:@"%@\n",text];
    
    NSData *data = [[NSData alloc] initWithData:[sendString dataUsingEncoding: NSUTF8StringEncoding]];
    [outputStream write:[data bytes] maxLength:[data length]];
    
    if (self.error == NO)
    {
        sendString = [sendString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    }
}

- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent {
    
    switch (streamEvent) {
            
        case NSStreamEventOpenCompleted:
            [self connected];
            break;
        case NSStreamEventHasBytesAvailable:
            
            if (theStream == inputStream) {
                
                uint8_t buffer[1024];
                int len;
                
                while ([inputStream hasBytesAvailable]) {
                    len = (int)[inputStream read:buffer maxLength:sizeof(buffer)];
                    if (len > 0) {
                        
                        NSString *output = [[NSString alloc] initWithBytes:buffer length:len encoding:NSUTF8StringEncoding];
                        
                        if (nil != output) {
                            
                            output = [output stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                            
                            if([output hasPrefix:@"Finished"])
                            {
                                NSLog(@"finishedAll");
                                [self stopTimer];
                                [self playSound];
                                [self startBlinkingLabel:self.elapsedTime];
                            }
                        }
                    }
                }
            }
            break;
            
        case NSStreamEventErrorOccurred:
            [self disconnect];
            break;
            
        case NSStreamEventHasSpaceAvailable:
            break;
            
        case NSStreamEventEndEncountered:
            [self disconnect];
            break;
            
        default:
            NSLog(@"Unknown event");
    }
}

-(void) startBlinkingLabel:(UILabel *)label
{
    label.alpha =1.0f;
    [UIView animateWithDuration:0.32
                          delay:0.0
                        options: UIViewAnimationOptionAutoreverse |UIViewAnimationOptionRepeat | UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction |UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         label.alpha = 0.0f;
                         label.backgroundColor = [UIColor redColor];
                     }
                     completion:^(BOOL finished){
                         if (finished) {
                             
                         }
                     }];
}

-(void) stopBlinkingLabel:(UILabel *)label
{
    // REMOVE ANIMATION
    [label.layer removeAnimationForKey:@"opacity"];
    label.alpha = 1.0f;
    label.backgroundColor = [UIColor clearColor];
}

- (IBAction) sliderValueChanged:(UISlider *)sender {
    self.sliderLabel.text = [NSString stringWithFormat:@"%.f", [sender value]];
    
    
    NSString *sendString = [NSString stringWithFormat:@"Motor;%.f\n",[sender value]];
    //[self sendString:stringToSend];
    
    NSData *data = [[NSData alloc] initWithData:[sendString dataUsingEncoding: NSUTF8StringEncoding]];
    [outputStream write:[data bytes] maxLength:[data length]];
    
}

- (IBAction) sliderValueAngleChanged:(UISlider *)sender {
    self.SliderAngleLabel.text = [NSString stringWithFormat:@"%.f", [sender value]];
    
    NSString *sendString = [NSString stringWithFormat:@"Angle;%.f\n",[sender value]];
    // [self sendString:sendString];
    
    NSData *data = [[NSData alloc] initWithData:[sendString dataUsingEncoding: NSUTF8StringEncoding]];
    [outputStream write:[data bytes] maxLength:[data length]];
    
}

- (IBAction)buttonWettbewerbClicked:(id)sender {
    
    NSString *stringToSend = @"Shoot;5";
    
    if (self.ballAmount.selectedSegmentIndex == 0) {
        stringToSend = @"Shoot;2";
        
    }

    [self sendString:stringToSend];
    [self startTimer];
}

- (IBAction)buttonVorbereitenClicked:(id)sender {
    NSString *stringToSend = @"Vorbereiten";
    
    [self sendString:stringToSend];
}

- (IBAction)buttonLadenClicked:(id)sender {
    NSString *stringToSend = @"Laden";
    
    [self sendString:stringToSend];
}

- (IBAction)buttonKlappeAufClicked:(id)sender {
    NSString *stringToSend = @"KlappeAuf";
    
    [self sendString:stringToSend];
}

- (IBAction)buttonKlappeZuClicked:(id)sender {
    NSString *stringToSend = @"KlappeZu";
    
    [self sendString:stringToSend];
}

- (void)connected{
    self.error = NO;
    
    [self.spinningConnect stopAnimating];
    self.checkBoxConnect.hidden = NO;
    self.checkBoxConnect.text = @"✅";
    
    [self.connectButton setTitle:@"Trennen" forState:UIControlStateNormal];
    
    self.buttonVorbereiten.enabled = YES;
    self.buttonKlappeAuf.enabled = YES;
    self.buttonKlappeZu.enabled = YES;
    self.buttonLaden.enabled = YES;
    self.buttonWettbewerb.enabled = YES;
    self.sliderToggleSwitch.enabled = YES;
    self.Slider.enabled = YES;
    self.SliderAngle.enabled = YES;
}
- (void)disconnect{
    self.connecting = NO;
    self.error = YES;
    
    [self stopTimer];
    [self.spinningConnect stopAnimating];
    self.checkBoxConnect.hidden = NO;
    self.checkBoxConnect.text = @"❌";
    
    [self.connectButton setTitle:@"Verbinden" forState:UIControlStateNormal];
    
    self.buttonVorbereiten.enabled = NO;
    self.buttonKlappeAuf.enabled = NO;
    self.buttonKlappeZu.enabled = NO;
    self.buttonLaden.enabled = NO;
    self.buttonWettbewerb.enabled = NO;
    self.sliderToggleSwitch.enabled = NO;
    self.Slider.enabled = NO;
    self.SliderAngle.enabled = NO;
    [self stopBlinkingLabel:self.elapsedTime];
    
    [self close];
}

-(IBAction)playSound {
    
    [audioPlayer play];
    
}
- (void)updateTimer
{
    // Create date from the elapsed time
    NSDate *currentDate = [NSDate date];
    NSTimeInterval timeInterval = [currentDate timeIntervalSinceDate:startDate];
    NSDate *timerDate = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    
    // Create a date formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"mm:ss:SS"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0.0]];
    
    // Format the elapsed time and set it to the label
    NSString *timeString = [dateFormatter stringFromDate:timerDate];
    self.elapsedTime.text = timeString;
}

- (void)startTimer{
    
    startDate = [NSDate date];
    
    // Create the stop watch timer that fires every 100 ms
    stopWatchTimer = [NSTimer scheduledTimerWithTimeInterval:1.0/10.0
                                                      target:self
                                                    selector:@selector(updateTimer)
                                                    userInfo:nil
                                                     repeats:YES];
}

- (void)stopTimer {
    [stopWatchTimer invalidate];
    stopWatchTimer = nil;
    [self updateTimer];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component
{
    return  _dataSourceArray.count;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row   inComponent:(NSInteger)component
{
    _actionString = _dataSourceArray[row];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    
    return [self.dataSourceArray objectAtIndex:row];
    
}



- (IBAction)sliderButton:(id)sender {
}
@end
